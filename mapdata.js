const DATA_URL = 'http://localhost:3000/data.csv';

var simplemaps_countrymap_mapdata = {
    main_settings: {
        //General settings
        width: "responsive", //'700' or 'responsive'
        background_color: "#FFFFFF",
        background_transparent: "yes",
        border_color: "#ffffff",

        //State defaults
        state_description: "State description",
        state_color: "#88A4BC",
        state_hover_color: "#3B729F",
        state_url: "",
        border_size: 1.5,
        all_states_inactive: "no",
        all_states_zoomable: "no",

        //Location defaults
        location_description: "Location description",
        location_url: "",
        location_color: "#FF0067",
        location_opacity: 0.8,
        location_hover_opacity: 1,
        location_size: 25,
        location_type: "square",
        location_image_source: "frog.png",
        location_border_color: "#FFFFFF",
        location_border: 2,
        location_hover_border: 2.5,
        all_locations_inactive: "no",
        all_locations_hidden: "no",

        //Label defaults
        label_color: "#d5ddec",
        label_hover_color: "#d5ddec",
        label_size: 22,
        label_font: "Arial",
        hide_labels: "no",
        hide_eastern_labels: "no",

        //Zoom settings
        zoom: "no",
        manual_zoom: "no",
        back_image: "no",
        initial_back: "no",
        initial_zoom: "-1",
        initial_zoom_solo: "no",
        region_opacity: 1,
        region_hover_opacity: 0.6,
        zoom_out_incrementally: "yes",
        zoom_percentage: 0.99,
        zoom_time: 0.5,

        //Popup settings
        popup_color: "white",
        popup_opacity: 0.9,
        popup_shadow: 1,
        popup_corners: 5,
        popup_font: "12px/1.5 Verdana, Arial, Helvetica, sans-serif",
        popup_nocss: "no",

        //Advanced settings
        div: "map",
        auto_load: "yes",
        url_new_tab: "yes",
        images_directory: "default",
        fade_time: 0.1,
        link_text: "View Website",
        popups: "detect",
        state_image_url: "",
        state_image_position: "",
        location_image_url: ""
    },
    locations: {},
    labels: {
        "0": {
            "parent_id": "ROU122",
            "x": 360.2,
            "y": 625
        },
        "1": {
            "parent_id": "ROU123",
            "x": 329,
            "y": 507.1
        },
        "2": {
            "parent_id": "ROU124",
            "x": 283.1,
            "y": 593.2
        },
        "3": {
            "parent_id": "ROU126",
            "x": 446.5,
            "y": 596.7
        },
        "4": {
            "parent_id": "ROU127",
            "x": 527.2,
            "y": 651.5
        },
        "5": {
            "parent_id": "ROU128",
            "x": 617.2,
            "y": 589.8
        },
        "6": {
            "parent_id": "ROU129",
            "x": 683,
            "y": 605.4
        },
        "7": {
            "parent_id": "ROU130",
            "x": 558.1,
            "y": 539
        },
        "8": {
            "parent_id": "ROU131",
            "x": 604.3,
            "y": 633.6
        },
        "9": {
            "parent_id": "ROU132",
            "x": 787.8,
            "y": 559.8
        },
        "10": {
            "parent_id": "ROU133",
            "x": 847.6,
            "y": 639.1
        },
        "11": {
            "parent_id": "ROU276",
            "x": 163.9,
            "y": 307.4
        },
        "12": {
            "parent_id": "ROU277",
            "x": 203.8,
            "y": 212.6
        },
        "13": {
            "parent_id": "ROU278",
            "x": 190.2,
            "y": 482.8
        },
        "14": {
            "parent_id": "ROU280",
            "x": 102.9,
            "y": 402.9
        },
        "15": {
            "parent_id": "ROU287",
            "x": 678.8,
            "y": 52.1
        },
        "16": {
            "parent_id": "ROU294",
            "x": 347.9,
            "y": 330.4
        },
        "17": {
            "parent_id": "ROU295",
            "x": 450.7,
            "y": 164.2
        },
        "18": {
            "parent_id": "ROU296",
            "x": 366.5,
            "y": 237.4
        },
        "19": {
            "parent_id": "ROU297",
            "x": 286.5,
            "y": 396.6
        },
        "20": {
            "parent_id": "ROU298",
            "x": 392.2,
            "y": 94.2
        },
        "21": {
            "parent_id": "ROU299",
            "x": 460,
            "y": 274.7
        },
        "22": {
            "parent_id": "ROU300",
            "x": 301.4,
            "y": 182.9
        },
        "23": {
            "parent_id": "ROU301",
            "x": 266.6,
            "y": 108.8
        },
        "24": {
            "parent_id": "ROU302",
            "x": 489.2,
            "y": 459
        },
        "25": {
            "parent_id": "ROU303",
            "x": 427.3,
            "y": 380.7
        },
        "26": {
            "parent_id": "ROU304",
            "x": 415.7,
            "y": 489
        },
        "27": {
            "parent_id": "ROU305",
            "x": 521.7,
            "y": 380.7
        },
        "28": {
            "parent_id": "ROU306",
            "x": 617,
            "y": 369.3
        },
        "29": {
            "parent_id": "ROU307",
            "x": 561.9,
            "y": 275.1
        },
        "30": {
            "parent_id": "ROU308",
            "x": 744.3,
            "y": 157.9
        },
        "31": {
            "parent_id": "ROU309",
            "x": 636,
            "y": 203.8
        },
        "32": {
            "parent_id": "ROU310",
            "x": 622.2,
            "y": 504.4
        },
        "33": {
            "parent_id": "ROU311",
            "x": 563.5,
            "y": 109.9
        },
        "34": {
            "parent_id": "ROU312",
            "x": 700.5,
            "y": 293.5
        },
        "35": {
            "parent_id": "ROU313",
            "x": 785.7,
            "y": 490.5
        },
        "36": {
            "parent_id": "ROU314",
            "x": 686.7,
            "y": 461.8
        },
        "37": {
            "parent_id": "ROU315",
            "x": 795.8,
            "y": 387.6
        },
        "38": {
            "parent_id": "ROU316",
            "x": 800.6,
            "y": 269.7
        },
        "39": {
            "parent_id": "ROU317",
            "x": 708,
            "y": 384.1
        },
        "40": {
            "parent_id": "ROU4844",
            "x": 625.1,
            "y": 578.9
        },
        "41": {
            "parent_id": "ROU4847",
            "x": 872.8,
            "y": 504.2
        }
    },
    regions: {}
};

function fetch_data() {
    const request = new XMLHttpRequest();
    request.open('GET', DATA_URL, false);
    request.send(null);
    if (request.status === 200) {
        return request.responseText;
    }
}

function read_data() {
    const csvData = fetch_data();
    const headerItems = [];
    const data = {};
    const lines = csvData.split('\n');
    for (const headerItem of lines[0].split(',')) {
        headerItems.push(headerItem);
    }
    const displayedItems = headerItems.slice(2, -1);
    for (let idx = 1; idx < lines.length; ++idx) {
        const values = lines[idx].split(',');
        const id = values[0];
        data[id] = {};
        for (let columnIdx = 1; columnIdx < headerItems.length; ++columnIdx) {
            data[id][headerItems[columnIdx]] = values[columnIdx];
        }
        data[id].description = displayedItems.map(item => `${data[id][item] || 0} ${item}`).join(', ');
    }
    simplemaps_countrymap_mapdata.state_specific = data;

    if (displayedItems.length !== 1) {
        simplemaps_countrymap_mapdata.labels = {};
    } else {
        const labels = simplemaps_countrymap_mapdata.labels;
        for (const key of Object.keys(labels)) {
            labels[key].name = data[labels[key].parent_id][displayedItems[0]];
        }
    }
}

read_data();
