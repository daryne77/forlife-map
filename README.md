Based on [SimpleMaps](https://simplemaps.com/custom/country/LHjRMEeh#start).

## Instructions

* Replace `DATA_URL` variable value in `mapdata.js:1`.
* The URL needs to provide a `.csv` file in the format specified in `sample_data.csv`.


## Installation

* [Potential installation steps for Wordpress](https://simplemaps.com/docs/wordpress-install).
